using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    private Dictionary<GameObject, Coroutine> burnDict = new Dictionary<GameObject, Coroutine>();

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent<IDamageable>(out var _damageable))
        {
            burnDict[col.gameObject] = StartCoroutine(IEBurn(_damageable));
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (burnDict.TryGetValue(other.gameObject, out var _coroutine))
        {
            StopCoroutine(_coroutine);
            burnDict.Remove(other.gameObject);
        }
    }

    IEnumerator IEBurn(IDamageable _damageable)
    {
        while (true)
        {
            _damageable.ApplyDamage(gameObject, 1);

            yield return new WaitForSeconds(0.5f);
        }
    }
}
