using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : Singleton<GameplayManager>
{
    private Dictionary<Type, BaseManager> managers = new Dictionary<Type, BaseManager>();

    public void AddManager<T>(T _manager) where T : BaseManager
    {
        managers[_manager.GetType()] = _manager;
    }

    public void RemoveManager<T>(T _manager) where T : BaseManager
    {
        if (managers.ContainsKey(_manager.GetType()))
        {
            managers.Remove(_manager.GetType());
        }
    }

    public bool TryGetManager<T>(out T _manager) where T : BaseManager
    {
        _manager = null;
        if (managers.TryGetValue(typeof(T), out var _resultManager))
        {
            _manager = (T) _resultManager;
        }
        return _manager;
    }
}
